# Source code for deterministic VL model (Le Rutte et al 2015 Parasit Vector),
# implemented in the pomp2 package (Partially observed Markov processes).

# Key assumptions:
#  - stochastic human population
#  - deterministic fly population
#  - age structured population

# 0. Initializing states -----------------------------------------------------


vl.init<- "
double *X = &SH_1;
double *initlocal = &init1;
#define init(K) initlocal[(K)]
int i;
int n = linitvec;

for(i=0; i<n; i++){
   X[i] = init(i);
  }

"

# 1. Deterministic model  ----------------------------------------------------


vl.code.det <- "

// Declare temporal variables.
int n = Nagecat; // number of age categories.
int i;           // indicator of age category.
int e = eRHC;    // indicator of number of erlang compartments for RHC.
double NH;       // local variable for total human population by age category.
double NHT;      // calculate total population size at time point.
double NHF;      // cumulative force of infection for lambdaF calculation.
double deaths;   // number of natural deaths at time point.
double lambdaH;  // force of infection acting on human population by age category.
double lambdaF;  // force of infection acting on fly population by age category.
double effectSEA; // effect of seasonality in fly abundance
double seaty; // time in year required to calculate the effectSEA variable
double seaTa; //seaT in years
double seaBla; //seaBla in years

//Define pointers to parameters vectors.
const double *muHlocal = &muH_1;  // mortality rate per person per day by age category.
const double *aexplocal = &aexp_1; // age dependant exposure.

//Define pointers to compartments.
 double *SHlocal = &SH_1;
 double *IHPlocal = &IHP_1;
 double *IHDlocal = &IHD_1;
 double *IHSlocal = &IHS_1;
 double *IHT1local = &IHT1_1;
 double *IHT2local = &IHT2_1;
 double *RHTlocal = &RHT_1;
 double *IHLlocal = &IHL_1;
 double *RHDlocal = &RHD_1;
 double *RHC1local = &RHC1_1;
 double *RHC2local = NULL;
 double *RHC3local = NULL;
 double *RHC4local = NULL;
 double *RHC5local = NULL;
 double *SFlocal = &SF;
 double *EFlocal = &EF;
 double *IFlocal = &IF;



//Define pointers to derivatives of compartments.
double *DSHlocal = &DSH_1;
double *DIHPlocal = &DIHP_1;
double *DIHDlocal = &DIHD_1;
double *DIHSlocal = &DIHS_1;
double *DIHT1local = &DIHT1_1;
double *DIHT2local = &DIHT2_1;
double *DRHTlocal = &DRHT_1;
double *DIHLlocal = &DIHL_1;
double *DRHDlocal = &DRHD_1;
double *DRHC1local = &DRHC1_1;
double *DRHC2local = NULL;
double *DRHC3local = NULL;
double *DRHC4local = NULL;
double *DRHC5local = NULL;
double *DSFlocal = &DSF;
double *DEFlocal = &DEF;
double *DIFlocal = &DIF;


//Define pointer for erlang RHC compartment.
if( e > 1 ){
    RHC2local = RHC1local + n;
    DRHC2local = DRHC1local + n;
    
    if( e== 5){
        RHC3local = RHC1local + 2*n;
        DRHC3local = DRHC1local + 2*n;
        RHC4local = RHC1local + 3*n;
        DRHC4local = DRHC1local + 3*n;
        RHC5local = RHC1local + 4*n;
        DRHC5local = DRHC1local + 4*n;
    }
 }




// Define functions to access to parameter vectors and to update compartment vector.
#define muH(K) muHlocal[(K)]
#define aexp(K) aexplocal[(K)]

#define SH(K) SHlocal[(K)]
#define IHP(K) IHPlocal[(K)]
#define IHD(K) IHDlocal[(K)]
#define IHS(K) IHSlocal[(K)]
#define IHT1(K) IHT1local[(K)]
#define IHT2(K) IHT2local[(K)]
#define RHT(K) RHTlocal[(K)]
#define IHL(K) IHLlocal[(K)]
#define RHD(K) RHDlocal[(K)]
#define RHC1(K) RHC1local[(K)]
#define RHC2(K) RHC2local[(K)]
#define RHC3(K) RHC3local[(K)]
#define RHC4(K) RHC4local[(K)]
#define RHC5(K) RHC5local[(K)]
#define SF(K) SFlocal[(K)]
#define EF(K) EFlocal[(K)]
#define IF(K) IFlocal[(K)]

#define DSH(K) DSHlocal[(K)]
#define DIHP(K) DIHPlocal[(K)]
#define DIHD(K) DIHDlocal[(K)]
#define DIHS(K) DIHSlocal[(K)]
#define DIHT1(K) DIHT1local[(K)]
#define DIHT2(K) DIHT2local[(K)]
#define DRHT(K) DRHTlocal[(K)]
#define DIHL(K) DIHLlocal[(K)]
#define DRHD(K) DRHDlocal[(K)]
#define DRHC1(K) DRHC1local[(K)]
#define DRHC2(K) DRHC2local[(K)]
#define DRHC3(K) DRHC3local[(K)]
#define DRHC4(K) DRHC4local[(K)]
#define DRHC5(K) DRHC5local[(K)]
#define DSF(K) DSFlocal[(K)]
#define DEF(K) DEFlocal[(K)]
#define DIF(K) DIFlocal[(K)]


// Epidemic process.

NHT = 0.0;
NHF = 0.0;
deaths = 0.0;

for (i = 0; i < n; i++){
    
    NH = SH(i) + IHP(i) + IHD(i) + IHS(i) + IHT1(i) + IHT2(i) + RHT(i) + IHL(i) + RHD(i) + RHC1(i);
        if(e>1){
            NH += RHC2(i);
            if(e==5){
            NH += RHC3(i)+ RHC4(i)+ RHC5(i);
             }
        }
    NHT += NH;
    NHF += (pIHP * IHP(i) + pIHD * IHD(i) + pIHS * IHS(i) + pIHT1 * IHT1(i) + pIHT2 * IHT2(i) + pIHL * IHL(i)) * aexp(i);
    deaths += muH(i) * NH + muK * IHS(i) + muKT * (IHT1(i) + IHT2(i));
    
}

lambdaF = beta * NHF / NHT;


for( i = 0; i < n; i++){

    // System of ODEs.

    lambdaH = beta * pH * IF(0) * aexp(i);
    

    DSH(i) = - (lambdaH + muH(i)) * SH(i);
    
        if (i == 0){
          if (birthop == 1){
            DSH(i) += deaths; // closed population (birth == death)          
          }else{
            DSH(i) += birthH * NHT; // births according to birth rate
          }
        }
      
        // add the RHC contribution according to erlang compartment model.
        if( e == 1){
            DSH(i) += (1 - fA) * rhoRHC * RHC1(i);
          }else if(e==2){
              DSH(i) += e * (1 - fA) * rhoRHC * RHC2(i);
            }else if(e==5){
                DSH(i) += e * (1 - fA) * rhoRHC * RHC5(i);}

        
    DIHP(i) = lambdaH * SH(i) - (rhoIHP + muH(i)) * IHP(i);
    
       // add the RHC contribution according to erlang compartment model.
        if( e == 1){
            DIHP(i) += fA * rhoRHC * RHC1(i);
          }else if(e==2){
              DIHP(i) += fA * rhoRHC * (RHC1(i) + RHC2(i));
            }else if(e==5){
                DIHP(i) += fA * rhoRHC * (RHC1(i) + RHC2(i) + RHC3(i) + RHC4(i) + RHC5(i));}
    
    DIHD(i) = rhoIHP * IHP(i) - (rhoIHD + muH(i)) * IHD(i);
    DIHS(i) = fS * rhoIHD * IHD(i) - (rhoIHS + muH(i) + muK) * IHS(i);
    DIHT1(i) = (1 - fP) * rhoIHS * IHS(i) - (rhoIHT1 + muH(i) + muKT) * IHT1(i);
    DIHT2(i) = fF * rhoIHT1 * IHT1(i) - (rhoIHT2 + muH(i) + muKT) * IHT2(i);
    DRHT(i) = fP * rhoIHS * IHS(i) + (1 - fF) * rhoIHT1 * IHT1(i) + rhoIHT2 * IHT2(i) - (rhoRHT + muH(i)) * RHT(i);
    DIHL(i) = fL * rhoRHT * RHT(i) - (rhoIHL + muH(i)) * IHL(i);
    DRHD(i) = (1 - fS) * rhoIHD * IHD(i) + (1 - fL) * rhoRHT * RHT(i) + rhoIHL * IHL(i) - (rhoRHD + muH(i)) * RHD(i);
    
    // DRHC according to erlang compartment model.
    
    DRHC1(i) = rhoRHD * RHD(i) - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH(i)) * RHC1(i);
        
        if( e > 1){
            DRHC2(i) = e * (1 - fA) * rhoRHC * RHC1(i) - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH(i)) * RHC2(i);
            
             if(e==5){
    
                DRHC3(i) = e * (1 - fA) * rhoRHC * RHC2(i) - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH(i)) * RHC3(i);
                DRHC4(i) = e * (1 - fA) * rhoRHC * RHC3(i) - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH(i)) * RHC4(i);
                DRHC5(i) = e * (1 - fA) * rhoRHC * RHC4(i) - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH(i)) * RHC5(i);
               }
        
         }
   }


//Transform current time in years for seasonal calculations
if(trate==0){
    seaty = t/365.0;
    seaTa = seaT/365.0;
    seaBla = seaBl/365.0;
    
}else{
    seaty = t;
}

if(seaType == 1){
    effectSEA = 1 + seaAmp * sin((seaty - seaTa) * M_2PI);
}else if(seaType==2){
    if(((seaty-floor(seaty)) > seaTa & (seaty-floor(seaty)) < (seaTa + seaBla))) {
        effectSEA= (1 + seaAmp) / (1 + 2 * seaAmp * seaBla - seaAmp);
      
    }else{
        effectSEA= (1 - seaAmp) / (1 + 2 * seaAmp * seaBla - seaAmp);
  
    }
    
}else {
    effectSEA = 1;
}

   DSF(0) = muF * NF * effectSEA * (1 - effectIRS) - (lambdaF + muF) * SF(0);
   DEF(0) = lambdaF * SF(0) - (rhoEF + muF) * EF(0);
   DIF(0) = rhoEF * EF(0) - muF * IF(0);

"

# 1.2. Deterministic aging function --------------------------------------------

eventfunc<- function(times, y, params) {

       
        n <- Nagecat
        y.h <- matrix(y[1:(length(y)-3)],nrow=nhstates,byrow=TRUE)
        y.f <- y[(length(y)-2):length(y)]
        
        if( n > 1){ # if there are more than 1 category aging is allowed
            
            ###vector of newborns
            if( birthop == 0){ #poisson distributed birth
                newborns <- c(birthH*sum(y.h)*ifelse(trate,1,365),rep(0,nhstates-1))
                }else if(birthop == 1 && deathop == 1){ #births=deaths & finite age
                    newborns <- c(sum(y.h[,n]),rep(0,nhstates-1))
                }
            ###vector of oldies
            if( deathop == 0){ #exponentially die
                oldies <- y.h[,n-1] + y.h[,n]
                }else{ # all die at the end
                    oldies <- y.h[,n-1]
                }
           }
        
           if( n > 2 ){
                    yn <- cbind(newborns,y.h[ ,1:(n-2)],oldies)
           }else{
                    yn <- cbind(newborns,oldies)}
           
           if( n > 1){
            y <- c(as.vector(t(yn)), y.f)
           }
            
        
        return(y)



}




# 2. Stochastic model  -------------------------------------------------------


vl.code.stoch <- "


// Declare temporal variables.
int n = Nagecat; // number of age categories.
int i;           // indicator of age category.
int tc1;
int tc2;
int e = eRHC;           // indicator of number of erlang compartments for RHC.

double NH;       // local variable for total human population by age category.
double NHT;      // calculate total population size at time point.
double NHF;      // cumulative force of infection for lambdaF calculation.

double lambdaH;  // force of infection acting on human population by age category.
double lambdaF;  // force of infection acting on fly population by age category.
double rate[40];  // Rates (28 = length, indexing starts at 0)
double dN[40];    // Derivatives (discrete number of persons that move between compartments)
double deaths;  //total of deaths per dt;

double effectSEA; // effect of seasonality in fly abundance
double seaty; // time in year required to calculate the effectSEA variable
double seaTa; //seaT in years
double seaBla; //seaBla in years


double SHtemp[n];
double IHPtemp[n];
double IHDtemp[n];
double IHStemp[n];
double IHT1temp[n];
double IHT2temp[n];
double RHTtemp[n];
double IHLtemp[n];
double RHDtemp[n];
double RHC1temp[n];
double RHC2temp[n];
double RHC3temp[n];
double RHC4temp[n];
double RHC5temp[n];
double VLtreatinctemp[n];
double VLinctemp[n];
double VLdeathtemp[n];
double SFtemp;
double EFtemp;
double IFtemp;

//Define pointers to parameter vectors.
double *muHlocal = &muH_1;  // mortality rate per person per day by age category
double *aexplocal = &aexp_1; // age dependant exposure.

//Define pointers to state vectors.
double *SHlocal = &SH_1;
double *IHPlocal = &IHP_1;
double *IHDlocal = &IHD_1;
double *IHSlocal = &IHS_1;
double *IHT1local = &IHT1_1;
double *IHT2local = &IHT2_1;
double *RHTlocal = &RHT_1;
double *IHLlocal = &IHL_1;
double *RHDlocal = &RHD_1;
double *RHC1local = &RHC1_1;
double *RHC2local = NULL;
double *RHC3local = NULL;
double *RHC4local = NULL;
double *RHC5local = NULL;
double *VLtreatinclocal = &VLtreatinc_1;
double *VLinclocal = &VLinc_1;
double *VLdeathlocal = &VLdeath_1;
double *SFlocal = &SF;
double *EFlocal = &EF;
double *IFlocal = &IF;



if( e > 1 ){
    RHC2local = RHC1local + n;
    
    if( e == 5 ){
        RHC3local = RHC1local + 2*n;
        RHC4local = RHC1local + 3*n;
        RHC5local = RHC1local + 4*n;
    }
}




// Define functions to interact with parameter and state vectors.
#define muH(K) muHlocal[(K)]
#define aexp(K) aexplocal[(K)]

#define SH(K) SHlocal[(K)]
#define IHP(K) IHPlocal[(K)]
#define IHD(K) IHDlocal[(K)]
#define IHS(K) IHSlocal[(K)]
#define IHT1(K) IHT1local[(K)]
#define IHT2(K) IHT2local[(K)]
#define RHT(K) RHTlocal[(K)]
#define IHL(K) IHLlocal[(K)]
#define RHD(K) RHDlocal[(K)]
#define RHC1(K) RHC1local[(K)]
#define RHC2(K) RHC2local[(K)]
#define RHC3(K) RHC3local[(K)]
#define RHC4(K) RHC4local[(K)]
#define RHC5(K) RHC5local[(K)]
#define VLtreatinc(K)  VLtreatinclocal[(K)]
#define VLinc(K)  VLinclocal[(K)]
#define VLdeath(K)  VLdeathlocal[(K)]
#define SF(K) SFlocal[(K)]
#define EF(K) EFlocal[(K)]
#define IF(K) IFlocal[(K)]


NHT = 0.0;
NHF = 0.0;

SFtemp = SF(0);
EFtemp = EF(0);
IFtemp = IF(0);

for (i = 0; i < n; i++){
    NH = SH(i) + IHP(i) + IHD(i) + IHS(i) + IHT1(i) + IHT2(i) + RHT(i) + IHL(i) + RHD(i) + RHC1(i);
    if( e > 1){
        NH += RHC2(i);
        if(e == 5){
            NH += RHC3(i)+ RHC4(i)+ RHC5(i);
        }
    }
    NHT += NH;
    NHF += (pIHP * IHP(i) + pIHD * IHD(i) + pIHS * IHS(i) + pIHT1 * IHT1(i) + pIHT2 * IHT2(i) + pIHL * IHL(i)) * aexp(i);
}

lambdaF = beta * NHF / NHT;

//Transform current time in years for seasonal calculations
if(trate==0){
    seaty = t/365.0;
    seaTa = seaT/365.0;
    seaBla = seaBl/365.0;
    
}else{
    seaty = t;
}

if(seaType == 1){
    effectSEA = 1 + seaAmp * sin((seaty - seaTa) * M_2PI);
}else if(seaType==2){
    if(((seaty-floor(seaty)) > seaTa & (seaty-floor(seaty)) < (seaTa + seaBla))) {
        effectSEA= (1 + seaAmp) / (1 + 2 * seaAmp * seaBla - seaAmp);
        //effectSEA=seaAmp;
    }else{
        effectSEA= (1 - seaAmp) / (1 + 2 * seaAmp * seaBla - seaAmp);
        //effectSEA=-seaAmp;
    }
    
}else {
    effectSEA = 1;
}

SF(0) += dt * (muF * NF * effectSEA * (1 - effectIRS) - (lambdaF + muF) * SFtemp);  // N.B.: effectIRS is plug-in covariate
EF(0) += dt * (lambdaF * SFtemp - (rhoEF + muF) * EFtemp);
IF(0) += dt * (rhoEF * EFtemp - muF * IFtemp);

deaths = 0.0;


for(i = 0; i < n; i++){
    
    lambdaH = beta * pH * IF(0) * aexp(i);
    // Transitions of discrete numbers of humans exiting each compartment
    // N.B. for the reulermultinom() function, exit rates for a given compartment
    // must be next to each other in the rate[] vector
    
    // SH (susceptibles)
    // DSH = ... - (lambdaH + muH) * SH;
    rate[0] = lambdaH;           // infection rate
    rate[1] = muH(i);               // death
    reulermultinom(2, SH(i), &rate[0], dt, &dN[0]);
    
    // IHP (infected, PCR+/DAT-)
    // DIHP = ... - (rhoIHP + muH) * IHP;
    rate[2] = rhoIHP;            // seroconversion rate
    rate[3] = muH(i);               // death
    reulermultinom(2, IHP(i), &rate[2], dt, &dN[2]);
    
    // IHD (infected, PCR+/DAT+)
    // DIHD = ... - (rhoIHD + muH) * IHD;
    rate[4] = fS * rhoIHD;       // progression to clinical symptoms
    rate[5] = (1-fS) * rhoIHD;   // spontaneous recovery without clinical symptoms
    rate[6] = muH(i);               // death
    reulermultinom(3, IHD(i), &rate[4], dt, &dN[4]);
    
    
    // IHS (clinical symptoms, no treatment, PCR+/DAT+)
    // DIHS = ... - (rhoIHS + muH + muK) * IHS;
    rate[7] = (1 - fP) * rhoIHS; // treatment rate
    rate[8] = fP * rhoIHS;       // spontaneous recovery from clinical symptoms
    rate[9] = muH(i);            // death due to other causes
    rate[10] = muK;             // death due to VL
    reulermultinom(4, IHS(i), &rate[7], dt, &dN[7]);
    
    // IHT1 (first treatment, PCR+/DAT+)
    // DIHT1 = ... - (rhoIHT1 + muH + muKT) * IHT1;
    rate[11] = fF * rhoIHT1;     // treatment failure
    rate[12] = (1-fF) * rhoIHT1; // treatment succes
    rate[13] = muH(i);           // death due to other causes
    rate[14] = muKT;        // death due to VL
    reulermultinom(4, IHT1(i), &rate[11], dt, &dN[11]);
    
    // IHT2 (second treatment, PCR+/DAT+)
    // DIHT2 = ... - (rhoIHT2 + muH + muKT) * IHT2;
    rate[15] = rhoIHT2;          // treatment succes
    rate[16] = muH(i);           // death due to other causes
    rate[17] = muKT;        // death due to VL
    reulermultinom(3, IHT2(i), &rate[15], dt, &dN[15]);
    
    // RHT (putatively recovered, PCR-/DAT+)
    // DRHT = ... - (rhoRHT + muH) * RHT;
    rate[18] = (1-fL) * rhoRHT;  // full recovery
    rate[19] = fL * rhoRHT;      // PKDL
    rate[20] = muH(i);           // death
    reulermultinom(3, RHT(i), &rate[18], dt, &dN[18]);
    
    // IHL (PKDL, PCR+/DAT+)
    // DIHL = ... - (rhoIHL + muH) * IHL;
    rate[21] = rhoIHL;           // full recovery
    rate[22] = muH(i);           // death
    reulermultinom(2, IHL(i), &rate[21], dt, &dN[21]);
    
    // RHD (recovered PCR-/DAT+)
    // DRHD = ... - (rhoRHD + muH) * RHD;
    rate[23] = rhoRHD;           // seroreversion rate
    rate[24] = muH(i);           // death
    reulermultinom(2, RHD(i), &rate[23], dt, &dN[23]);
    
    // RHC1 (recovered, PCR-/DAT-)
    // DRHC1 = ... - (rhoRHC + muH) * RHC
    rate[25] = fA * rhoRHC;      // reactivation rate
    rate[26] = e * (1-fA) * rhoRHC;  // loss of immunity
    rate[27] = muH(i);           // death
    reulermultinom(3, RHC1(i), &rate[25], dt, &dN[25]);
    
    if(e > 1){
        // RHC2 (recovered, PCR-/DAT-) - Erlang compartment 2
        // DRHC2 = ... - (2 * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC2;
        rate[28] = fA * rhoRHC;          // reactivation rate (competing risk)
        rate[29] = e * (1-fA) * rhoRHC;  // final stage of loss of immunity
        rate[30] = muH(i);                  // death
        reulermultinom(3, RHC2(i), &rate[28], dt, &dN[28]);
        
        if(e == 5){
            
            // RHC3 (recovered, PCR-/DAT-) - Erlang compartment 3
            // DRHC3 = ... - (5 * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC3;
            rate[31] = fA * rhoRHC;          // reactivation rate (competing risk)
            rate[32] = e * (1-fA) * rhoRHC;  // third stage of loss of immunity
            rate[33] = muH(i);                  // death
            reulermultinom(3, RHC3(i), &rate[31], dt, &dN[31]);
            
            // RHC4 (recovered, PCR-/DAT-) - Erlang compartment 4
            // DRHC4 = ... - (5 * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC4;
            rate[34] = fA * rhoRHC;          // reactivation rate (competing risk)
            rate[35] = e * (1-fA) * rhoRHC;  // fourth stage of loss of immunity
            rate[36] = muH(i);                  // death
            reulermultinom(3, RHC4(i), &rate[34], dt, &dN[34]);
            
            // RHC5 (recovered, PCR-/DAT-) - Erlang compartment 5
            // DRHC5 = ... - (5 * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC5;
            rate[37] = fA * rhoRHC;          // reactivation rate (competing risk)
            rate[38] = e * (1-fA) * rhoRHC;  // final stage of loss of immunity
            rate[39] = muH(i);                  // death
            reulermultinom(3, RHC5(i), &rate[37], dt, &dN[37]);
            
            
        }
        
    }
 
 // System of ODEs
 //DSH = muH * NH + muK * IHS + muKT * (IHT1 + IHT2) + (1 - fA) * rhoRHC * RHC - (lambdaH + muH) * SH;
 deaths += dN[1] + dN[3] + dN[6] + dN[9] + dN[10]+ dN[13] + dN[14]+ dN[16] + dN[17]+ dN[20] + dN[22] + dN[24] + dN[27];
 
 
 if( e > 1){
     deaths += dN[30];
     
     if( e == 5){
         deaths += dN[33] + dN[36] + dN[39];
     }
 }
 
 
 SH(i) += - dN[1] - dN[0];  // dN[1] terms cancel out
 
 if( e == 1){
     SH(i) += dN[26];
 }else if ( e == 2){
     SH(i) += dN[29];
 }else if ( e == 5){
     SH(i) += dN[38];
 }
 
 SHtemp[i] = SH(i);
 
 //DIHP = lambdaH * SH + fA * rhoRHC * RHC - (rhoIHP + muH) * IHP;
 IHP(i) += dN[25] + dN[0] - dN[2] - dN[3];
 
 if( e > 1){
     IHP(i) += dN[28];
     if ( e == 5){
         IHP(i) += dN[31] + dN[34] + dN[37];
     }
 }
 
 IHPtemp[i] = IHP(i);
 
 //DIHD = rhoIHP * IHP - (rhoIHD + muH) * IHD;
 IHD(i) += dN[2] - dN[4] - dN[5] - dN[6];
 IHDtemp[i] = IHD(i);
 
 //DIHS = fS * rhoIHD * IHD - (rhoIHS + muH + muK) * IHS;
 IHS(i) += dN[4] - dN[7] - dN[8] - dN[9] - dN[10];
 IHStemp[i] = IHS(i);
 
 //DIHT1 = (1 - fP) * rhoIHS * IHS - (rhoIHT1 + muH + muKT) * IHT1;
 IHT1(i) += dN[7] - dN[11] - dN[12] - dN[13] - dN[14];
 IHT1temp[i] = IHT1(i);
 
 //DIHT2 = fF * rhoIHT1 * IHT1 - (rhoIHT2 + muH + muKT) * IHT2;
 IHT2(i) += dN[11] - dN[15] - dN[16] - dN[17];
 IHT2temp[i] = IHT2(i);
 
 //DRHT = fP * rhoIHS * IHS + (1 - fF) * rhoIHT1 * IHT1 + rhoIHT2 * IHT2 - (rhoRHT + muH) * RHT;
 RHT(i) += dN[8] + dN[12] + dN[15] - dN[18] - dN[19] - dN[20];
 RHTtemp[i] = RHT(i);
 
 //DIHL = fL * rhoRHT * RHT - (rhoIHL + muH) * IHL;
 IHL(i) += dN[19] - dN[21] - dN[22];
 IHLtemp[i] = IHL(i);
 
 //DRHD = (1 - fS) * rhoIHD * IHD + (1 - fL) * rhoRHT * RHT + rhoIHL * IHL - (rhoRHD + muH) * RHD;
 RHD(i) += dN[5] + dN[18] + dN[21] - dN[23] - dN[24];
 RHDtemp[i] = RHD(i);
 
 //DRHC1 = rhoRHD * RHD - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC1;
 RHC1(i) += dN[23] - dN[25] - dN[26] - dN[27];
 RHC1temp[i] = RHC1(i);
 
 if (e > 1){
     //DRHC2 = e * (1 - fA) * rhoRHC * RHC1 - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC2;
     RHC2(i) += dN[26] - dN[28] - dN[29] - dN[30];
     RHC2temp[i] = RHC2(i);
     
     if (e == 5){
         //DRHC3 = e * (1 - fA) * rhoRHC * RHC2 - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC3;
         RHC3(i) += dN[29] - dN[31] - dN[32] - dN[33];
         RHC3temp[i] = RHC3(i);
         
         //DRHC4 = e * (1 - fA) * rhoRHC * RHC3 - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC4;
         RHC4(i) += dN[32] - dN[34] - dN[35] - dN[36];
         RHC4temp[i] = RHC4(i);
         
         //DRHC5 = e * (1 - fA) * rhoRHC * RHC4 - (e * (1 - fA) * rhoRHC + fA * rhoRHC + muH) * RHC5;
         RHC5(i) += dN[35] - dN[37] - dN[38] - dN[39];
         RHC5temp[i] = RHC5(i);
     }
 }
 
 
 //Values were divided by delta in order to get VL incidence in day or years.
 
 VLtreatinc(i) += dN[7];  // new treated VL cases
 VLtreatinctemp[i] = VLtreatinc(i);
 
 VLinc(i) += dN[4];        // new VL cases (untreated)
 VLinctemp[i] = VLinc(i);
 
 VLdeath(i) += dN[10];     // VL deaths
 VLdeath(i) += dN[14];
 VLdeath(i) += dN[17];
 VLdeathtemp[i] = VLdeath(i);
 
 //Conditions to get the time point where aging arrives.
 
 if ( trate == 0){
     tc1 = floor((t)/365);
     tc2 = floor((t-dt)/365);
 }else{
     tc1 = floor((t));
     tc2 = floor((t-dt));
 }
 
 
 if( ((tc1 - tc2) > 0.5) && (n > 1) && (t > 0)){  // if it's the turn of the year
     
     if(i == 0){
         
         // update newborns
         if ( birthop == 0){ // poisson distributed birth
             if( trate == 1){ // if use annually rates
                 SH(i) = rpois((birthH * NHT));
             }else{ // if use daily rates
                 SH(i) = rpois((birthH * NHT * 365));}
         }else if( birthop == 1 && deathop == 1){ // closed system (births == deaths)
             SH(i) = NH;
             // NH contains the nubmer of human in the highest age at this point
             // (who are about to die if deathop == 1 (i.e. finite age); if
             // deathop==0 (exponential death in highest age category), these 
             // individuals will die via the regulare route).
         }
         
         IHP(i) = 0;
         IHD(i) = 0;
         IHS(i) = 0;
         IHT1(i) = 0;
         IHT2(i) = 0;
         RHT(i) = 0;
         IHL(i) = 0;
         RHD(i) = 0;
         RHC1(i) = 0;
         
         if ( e > 1){
             RHC2(i) = 0;
             if (e == 5){
                 RHC3(i) = 0;
                 RHC4(i) = 0;
                 RHC5(i) = 0;
             }
         }
         
         VLtreatinc(i) = 0;
         VLinc(i) = 0;
         VLdeath(i) = 0;
         
     } else if (i == (n-1)){
         
         // update oldies
         if ( deathop == 1){ // all die at the end
             SH(i) = SHtemp[i-1];
             IHP(i) = IHPtemp[i-1];
             IHD(i) = IHDtemp[i-1];
             IHS(i) = IHStemp[i-1];
             IHT1(i) = IHT1temp[i-1];
             IHT2(i) = IHT2temp[i-1];
             RHT(i) = RHTtemp[i-1];
             IHL(i) = IHLtemp[i-1];
             RHD(i) = RHDtemp[i-1];
             RHC1(i) = RHC1temp[i-1];
             if ( e > 1){
                 RHC2(i) = RHC2temp[i-1];
                 if (e == 5){
                     RHC3(i) = RHC3temp[i-1];
                     RHC4(i) = RHC4temp[i-1];
                     RHC5(i) = RHC5temp[i-1];
                 }
             }
             
             VLtreatinc(i) = VLtreatinctemp[i-1];
             VLinc(i) = VLinctemp[i-1];
             VLdeath(i) = VLdeathtemp[i-1];
             
         } else { //exponential death
             SH(i) = SHtemp[i-1] + SHtemp[i];
             IHP(i) = IHPtemp[i-1] + IHPtemp[i];
             IHD(i) = IHDtemp[i-1] + IHDtemp[i];
             IHS(i) = IHStemp[i-1] + IHStemp[i];
             IHT1(i) = IHT1temp[i-1] + IHT1temp[i];
             IHT2(i) = IHT2temp[i-1] + IHT2temp[i];
             RHT(i) = RHTtemp[i-1] + RHTtemp[i];
             IHL(i) = IHLtemp[i-1] + IHLtemp[i];
             RHD(i) = RHDtemp[i-1] + RHDtemp[i];
             RHC1(i) = RHC1temp[i-1] + RHC1temp[i];
             if ( e > 1){
                 RHC2(i) = RHC2temp[i-1] + RHC2temp[i];
                 if (e == 5){
                     RHC3(i) = RHC3temp[i-1] + RHC3temp[i];
                     RHC4(i) = RHC4temp[i-1] + RHC4temp[i];
                     RHC5(i) = RHC5temp[i-1] + RHC5temp[i];
                 }
             }
             
             VLtreatinc(i) = VLtreatinctemp[i-1] + VLtreatinctemp[i];
             VLinc(i) = VLinctemp[i-1] + VLinctemp[i];
             VLdeath(i) = VLdeathtemp[i-1] + VLdeathtemp[i];
         }
         
     } else {  // update the rest of the age categories
         SH(i) = SHtemp[i-1];
         IHP(i) = IHPtemp[i-1];
         IHD(i) = IHDtemp[i-1];
         IHS(i) = IHStemp[i-1];
         IHT1(i) = IHT1temp[i-1];
         IHT2(i) = IHT2temp[i-1];
         RHT(i) = RHTtemp[i-1];
         IHL(i) = IHLtemp[i-1];
         RHD(i) = RHDtemp[i-1];
         RHC1(i) = RHC1temp[i-1];
         if ( e > 1){
             RHC2(i) = RHC2temp[i-1];
             if (e == 5){
                 RHC3(i) = RHC3temp[i-1];
                 RHC4(i) = RHC4temp[i-1];
                 RHC5(i) = RHC5temp[i-1];
             }
         }
         VLtreatinc(i) = VLtreatinctemp[i-1];
         VLinc(i) = VLinctemp[i-1];
         VLdeath(i) = VLdeathtemp[i-1];
     }
 }
 
}

//Demographic birth update


if( ((tc1 - tc2) > 0.5)&&(n > 1)&& (t > 0)){ // if it's the turn of the year...
    
    if( birthop == 0){  // births according to birth rate
       SH(1) = SH(1) + rpois((birthH * NHT * dt));
    }else{  // closed population (births == deaths)
       SH(1) = SH(1) + deaths;
    }
    
}else{  // or when just another day in the same year
    
    if( birthop == 0){  // births according to birth rate
       SH(0) = SH(0) + rpois((birthH * NHT * dt));
    }else{  // closed population (births == deaths)
       SH(0) = SH(0) + deaths;
    }
    
}


"




### END OF CODE ###




