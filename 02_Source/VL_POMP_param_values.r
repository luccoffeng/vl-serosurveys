# Set parameter values (all rates defined per day, seasonal parameters in days) and initial states

#mu.H lacks!!
# Number of yearly age categories (minimum = 2).
# N.B.: Individuals will accumulate in the highest age category, where they the next day

# Load and process demographic data
demogr<-fread(file=file.path(source.dir,"India_estimated_death_2006_2011.csv"))


param.values=list(

# Transition rates for human states
    birthH = 21 / 1e3/ 365,    # Birth rate per capita per day in 2011
    muK = 1/150,               # Excess mortality rate during untreated symptomatic infection
    muKT = 1/120,              # D 1/150 .Excess death rate during treated symptomatic infection
    rhoIHP = 1/60,             # 1/Duration of asymptomatic 1 (PCR+, DAT-)
    rhoIHD = 1/12,             # 1/Duration of asymptomatic 2 (PCR+, DAT+)
    rhoIHS = 1/60,             # D=0.0222   #. 1/Duration of symptomatic untreated
    rhoIHT1 = 1/1,             # D0.4  .1/Duration of treatment 1
    rhoIHT2 = 1/28,            # D 0.1 .1/Duration of treatment 2
    rhoRHT = 1/(21*365/12),    # 1/Duration of putatively recovered (21 months)
    rhoIHL = 1/(5*365),        # 1/Duration PKDL (5 years)
    rhoRHD = 1/74,             # 1/Duration of recovered 1 (PCR-, DAT+) # swaped
    rhoRHC = 1/307,            # 1/Duration of recovered 2 (PCR-, DAT-)

# Relative infectivity of humans (towards sandflies)
    pIHP  = 0.0125,            # Infectivity of asymptomatically infected humans 1 (PCR+, DAT-)
    pIHD  = 0.025,             # Infectivity of asymptomatically infected humans 2 (PCR+, DAT+)
    pIHS  = 1,                 # Infectivity of aymptomatic untreated
    pIHT1 = 0.5,               # Infectivity of treatment 1 (assuming infectivity starts at one and decline to zero)
    pIHT2 = 0.5,               # Infectivity of treatment 2 (assuming infectivity starts at one and decline to zero)
    pIHL  = 0.9,               # D0.9 Infectivity of PKDL
   #p.RHT = 0,                 # Putatively recovered
   #p.RHD = 0,                 # Recovered 1 (PCR-, DAT+)
   #p.RHC = 0,                 # Recovered 2 (PCR-, DAT-)



# Fractions of flows between in human states
    fA = 0,                   # Fraction of recovered 2 in whom infection reactivates
    fP = 1/34,                # Fraction symptomatic untreated to putatively recovered (same as Stauch)
    fL = 0.025,               # D.0.05 Fraction putatively recovered to PKDL (Stauch = 1/34, 0.2 results in very high prevalences)
    fS = 0.00385,             # Fraction asymptomatic to symptomatic untreated (adjused to arrive at incidence of VL)
    fF =  0.1134,             # Fraction going from treatment 1 to treatment 2


 # Fly-related rates
    NF    = 5.27,             # Number of flies per human (0.451) 0.512
                              # Model E1 IHT1 incidence od 10 / 10,000/year
                              # 1.960 -> E0 VL incidence 25 / 10,000 capita (IHS incidence)
                              # 1.960 -> E0 VL incidence 25 / 10,000 capita (IHS incidence)
                              # 1.493 -> E0 VL incidence 5 / 10,000 capita (IHS incidence)
    muF   = 1/14,             # 1/Life expectancy of sandfly
    pH    = 1,                # Transmission probability of infectious fly to human
    beta  = 1/4,              # Biting rate (1/time between consecutive bites)
    rhoEF = 1/5,              # 1/Duration of exposed stage of infected flies

# Seasonality in sandfly abundance
 
    seaType = 0,              # Seasonal option 0:None, 1:Sinoidal 2:Step-wise
    seaAmp  = 1,              # Relative amplitude (has to between 0.0 and 1.0)
    seaT    = 6 / 12 * 365,   # Time of peak vector abundance
    seaBl   = 3 / 12 * 365,   # Season block (Duration of the peaks)


# Age extension parameters
    Nagecat  = 90,            # Number of age categories
    birthop  = 1,             # 0: birth = Poisson-distributed, based on birthH
                              # 1: birth = deaths (closed population)
    deathop  = 1,             # 0: exponential death in the last age category
                              # 1: immediate death after reaching end of last category (finite age)
    agexop   = 1,             # age-dependend exposure: 1:yes 0: no
    eRHC     = 1,             # Number of Erlang compartments for RHC {1,2,5}
    trate    = 0,             # 0 if rates (rho,mu,beta and birth rates (birthH,mu.H) are provided by user in days, 1 in years.
    Ncluster = 1              # 1 village study
  )

    #mu.H <- rep(1/(68.1*365), 90)

#Mortality rate per age
    mort.age <- unlist(mapply(rep, demogr[, `Total-2011`], c(1, 4, rep(5, 16), 10)))
    mort.age <- mort.age / 1000 / 365  # Mortality rate per capita per day
    mu.H =  mort.age[1:90]


#Approximate age-distribution of population in equilibium and in absence of
# excess mortality (to initialize demography)
    NH <- exp(cumsum(-mu.H) * 365)
    NH <- NH / sum(NH)  # normalize so total human population size is 1.0



### END OF CODE ###
  
