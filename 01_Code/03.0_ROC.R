#  Plot the ROC curve from the results from step (2b).



# 0. Libraries and sources ----
rm(list=ls())
library(pomp)
library(data.table)
library(rstudioapi)  # requires RStudio
library(ggplot2)
library(foreach)
library(doParallel)
library(ROCR)


# 0.1 Define directory paths. ----
base.dir <- dirname(dirname(getActiveDocumentContext()$path))
code.dir <- file.path(base.dir, "01_Code")
source.dir <- file.path(base.dir, "02_Source")
output.dir <- file.path(base.dir, "03_Output")
graph.dir <- file.path(base.dir, "04_Graphs")

# 0.2 Load data ----
load(file = file.path (output.dir, "Trajectory10afterTNC.RData" ))
load(file=file.path(output.dir,"Timenocases.Rdata")) #load timenocases dataset 

# Merge ID numbers and pre-control VL incidence
Timenocases <- Timenocases[!is.na(time)]
T10ATNC[, model := factor(Timenocases[T10ATNC$.id, model])]
T10ATNC[, BLVLtreatinc_p := Timenocases[T10ATNC$.id, BLVLtreatinc_p]]


#1. Determine conditions----
sumtable <- T10ATNC[time>365,.(NTC = sum(VLinc, na.rm = TRUE),
                       NRC = sum(VLtreatinc, na.rm = TRUE)),
                    by = .(model, .id)]
sumtable <- merge( sumtable, T10ATNC[ ,.SD[.N], by = .id,.SDcols = c("IF_p")], by = ".id")
sumtable[, NTCcond := ifelse( NTC != 0,"Yes","No")] #a new true VL case
sumtable[, NRCcond := ifelse( NRC != 0,"Yes","No")] #a new reported VL case
sumtable[, IFcond := ifelse( IF_p > 1e-5,"Yes","No")] #IF/NF was > 1e-5 at the end of the simulation



# 2.1. Data of prevalences 1 year after stopping control and condition ----
data <- merge(T10ATNC[time==365,c(".id", "time", "DAT_p","DAT_1t5","DAT_6t15","DAT_15p",        
                                   "IHD_p", "IHD_1t5","IHD_6t15","IHD_15p")],
              sumtable[,c(".id", "NTCcond", "NRCcond", "IFcond")],by=".id" )
data_l <- melt( data , id.vars = c( ".id", "time", "NTCcond", "NRCcond", "IFcond") )
data_l[,marker:=gsub("_.*","",variable)]
data_l[,aggregation:=gsub(".*_","",variable)]
data_l [ , population := fcase(     aggregation == "p","All",
                                    aggregation == "1t5","1-5",
                                    aggregation == "6t15","6-15",
                                    aggregation == "15p","15+")]
data_l$population <- factor( data_l$population, levels = c("1-5", "6-15", "15+", "All") )
sum_data_l<-data_l[,.(min=min(value), max=max(value)),by="variable"]

data_l[,NTCcond:= NTCcond=="Yes"]
data_l[,NRCcond:= NRCcond=="Yes"]
data_l[,IFcond:= IFcond=="Yes"]

# 2.2. Get the specificity and sentivity data----
ROCfunc<-function(x){
  datas<-data_l[variable==x][order(value)]
  for(varv in c("NTCcond","NRCcond","IFcond")){
    datas[,paste0("pos_",varv):=sum(get(varv))]
    datas[,paste0("neg_",varv):=sum(!get(varv))]
    datas[,paste0("tn_",varv):=cumsum(!get(varv))]
    datas[,paste0("spec_",varv):=get(paste0("tn_",varv))/get(paste0("neg_",varv))]
    datas[,paste0("tp_",varv):=get(paste0("pos_",varv))-cumsum(get(varv))]
    datas[,paste0("sens_",varv):=get(paste0("tp_",varv))/get(paste0("pos_",varv))]
  }
 return(datas) 
}


dataROC<-do.call(rbind,
                lapply(X=c("DAT_p","DAT_1t5","DAT_6t15","DAT_15p","IHD_p", "IHD_1t5","IHD_6t15","IHD_15p"),
                       FUN=ROCfunc))
dataROC$threshold=dataROC$value

dataROC_long <- melt(setDT(dataROC[,c(".id","variable","marker","aggregation","population","threshold",
                              "spec_NTCcond","sens_NTCcond",
                              "spec_NRCcond","sens_NRCcond",
                              "spec_IFcond","sens_IFcond")]), 
             id.vars = c(".id","variable","marker","aggregation","population","threshold"), 
             variable.name = "var_ROC1")

dataROC_long[,var_ROC:=gsub("_.*","",var_ROC1)]
dataROC_long[,condition:=gsub(".*_","",var_ROC1)]
dataROC_long[,var_ROC1:=NULL]



dataROC_wide=setDT(dcast(dataROC_long,.id+variable+marker+aggregation+population+threshold+condition~var_ROC ))
dataROC_wide[,Population:=population]
dataROC_wide$condition<-factor(dataROC_wide$condition,level=c("NTCcond","NRCcond","IFcond"))
levels(dataROC_wide$condition)<-c("New true case?","New reported case?","IF/NF>1e5?")

#2.3.Threshold point dataset----
thres0<-setDT(dataROC_wide[order(threshold)])[threshold>0.0001,.SD[1],by=.(marker,condition,Population)]
thres0$Threshold<-"0.01"
thres1<-setDT(dataROC_wide[order(threshold)])[threshold>0.0005,.SD[1],by=.(marker,condition,Population)]
thres1$Threshold<-"0.05"
thres2<-setDT(dataROC_wide[order(threshold)])[threshold>0.001,.SD[1],by=.(marker,condition,Population)]
thres2$Threshold<-"0.1"
thres3<-setDT(dataROC_wide[order(threshold)])[threshold>0.005,.SD[1],by=.(marker,condition,Population)]
thres3$Threshold<-"0.5"
thres<-rbind(thres0,thres1,thres2,thres3)

#3. ROC plot----
ggplot(dataROC_wide,aes(x=1-spec,y=sens,color=Population))+
  geom_line()+
  facet_grid(marker~condition)+
  theme_light()+
  geom_abline(intercept = 0, slope = 1,linetype=2,color="black")+
  coord_equal(ratio = 1)+
  geom_point(data=thres,aes(x=1-spec,y=sens,color=Population,shape=Threshold))+
  xlab("1-Specificity")+ylab("Sensitivity")+
  theme(axis.text.x = element_text(size=5),
        axis.text.y = element_text(size=5))+
  scale_x_continuous(breaks = seq(0,1, by =0.2))+
  scale_y_continuous(breaks = seq(0,1, by =0.2))
ggsave(file.path(graph.dir,"ROC.pdf"), width = 6, height = 6)


#4. Prevalence

data_l2 <- melt(setDT(data_l), id.vars = c('.id','time','variable','value','marker','aggregation','population'), 
             variable.name = "condition",
             value.name="valcond")


ggplot(data_l2[marker=="DAT"], aes(x = value*100)) +
  geom_histogram(aes( fill = valcond),
                 position = "identity", bins = 50,alpha=0.4) +
  facet_grid(population~condition)+
  theme_light()+xlab("Marker prevalence(%)")+
  ggtitle("Histogram of DAT marker")
ggsave(file.path(graph.dir,"DAT_Density.pdf"), width = 6, height = 6)


ggplot(data_l2[marker=="IHD"], aes(x = value*100)) +
  geom_histogram(aes( fill = valcond),
                 position = "identity", bins = 50,alpha=0.4) +
  facet_grid(population~condition)+
  theme_light()+xlab("Marker prevalence(%)")+
  ggtitle("Histogram of IHD marker")+
  scale_x_continuous(breaks = seq(0,1, by =0.2))+
ggsave(file.path(graph.dir,"IHD_Density.pdf"), width = 6, height = 6)

