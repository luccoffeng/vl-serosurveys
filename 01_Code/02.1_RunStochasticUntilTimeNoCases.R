
#2a) Rerun the exact same simulations from the previous step (i.e. using the same value of NF and the same random number seed for pomp), but only the simulations that did not result in censoring. Run each simulation only up to the exact time point when no VL cases were reported for three years.

# 0. Libraries and sources ----
library(pomp)
library(data.table)
library(rstudioapi)  # requires RStudio
library(ggplot2)
library(foreach)
library(doParallel)

# 0.1 Define directory paths. ----
base.dir <- dirname(dirname(getActiveDocumentContext()$path))
code.dir <- file.path(base.dir, "01_Code")
source.dir <- file.path(base.dir, "02_Source")
output.dir <- file.path(base.dir, "03_Output")
graph.dir <- file.path(base.dir, "04_Graphs")

source(file = file.path(source.dir, "VL_POMP_helper_functions.r"))
source(file = file.path(source.dir, "VL_POMP_param_values.r"))
source(file = file.path(source.dir, "VL_POMP_init_values.r"))
source(file = file.path(source.dir, "VL_POMP_age_model.r")) #age dependance det and sto codes multi-village
load(file = file.path(output.dir, "Timenocases.Rdata")) #load timenocases dataset
#duration of delay and duration of mitigation.


#0.2.Load table of alternative parameter sets.----
param.values0 <- param.values
par.alt <- as.data.table(read.csv(file=file.path(source.dir, "param_sets.csv")))

#0.3.Set alternative parameters ----
popsize0 <- Timenocases[1, popsize0]
Nagecat <- 90
eRHC <- 1 #exponential model

#time frame of control
start.time <- 0
end.time <- 20*365  
delta <- 365/12
time.window <- seq(start.time, end.time, by = delta)

#Stochastic model parameters
time.step <- 1
nsim <- 1

#0.4.Load precontrol table ----
fit.ext<- read.csv(file=file.path(output.dir, "fit.ext.csv")) #table with NF at equilibrium
load(file = file.path(output.dir, "Initcontrol.RData")) #states at pre control
allopt <- as.data.table(merge(fit.ext[,-1], par.alt, by="model")) # par.alt with NF at equilibrium
param.values0 <- param.values

allopt <- allopt[Timenocases[, !is.na(time)]]
Timenocases <- Timenocases[!is.na(time)]
Timenocases[,Timenocases:=time]

#2. Get the pomp objects for scenarios of mitigation model


#2.1 Run control period one example

# 2.1.1. Update parameter list with the alternative parameters ----  

param.values0[colnames(allopt[,c(5:13)])] <- allopt[1,c(5:13)]
param.values0$NF <- allopt[1,]$NF0
param.values0$rhoIHS <- NULL
param.values0$effectIRS <- NULL

# 2.1.2. Extend the parameter list in age structure format ----

param.values.ext<-extend.param.age( param.values = param.values0,
                                    eRHC = 1, #Exponential model
                                    initvec = as.numeric(Initcontrol[[1]][1,]),
                                    b.age = NULL, #The defined function is the default one
                                    mu.H = mu.H, #
                                    probcat = NH,
                                    popsize = popsize0)

param.values.det <- param.values.ext[[1]]
param.values.stoch <- param.values.ext[[2]]
statenames.det <- param.values.ext[[3]]
statenames.stoch <- param.values.ext[[4]]
counternames.stoch<-param.values.ext[[5]]


#2.2 Get the pomp object

control.start <- 0

timev <- c(control.start-100,control.start,control.start+5,control.start+100)*365
rhoIHSv <- c(1/60,1/45,1/30,1/30)
effectIRSv <- c(0,0.67,0.45,0.45)


policy_E0 <-covariate_table(effectIRS = effectIRSv*0.999,
                            rhoIHS=rhoIHSv,
                            order = "constant",
                            times = timev)

policy_E1 <-covariate_table(effectIRS = effectIRSv*0.829,
                            rhoIHS=rhoIHSv,
                            order = "constant",
                            times = timev)

pomp.stoch_E0 <- pomp(data = data.frame(time = time.window),
                      times = "time",
                      t0 = start.time,
                      rinit = Csnippet(vl.init),
                      rprocess = euler(step.fun = Csnippet(vl.code.stoch),
                                       delta.t = time.step),
                      statenames = statenames.stoch,
                      paramnames = names(param.values.stoch),
                      accumvars = counternames.stoch,
                      covar = policy_E0,
                      cdir=".", cfile="pomp.stoch")

pomp.stoch_E1 <- pomp(data = data.frame(time = time.window),
                      times = "time",
                      t0 = start.time,
                      rinit = Csnippet(vl.init),
                      rprocess = euler(step.fun = Csnippet(vl.code.stoch),
                                       delta.t = time.step),
                      statenames = statenames.stoch,
                      paramnames = names(param.values.stoch),
                      accumvars = counternames.stoch,
                      covar = policy_E1,
                      cdir=".", cfile="pomp.stoch")


#3 Get the control history 


cluster <- makeCluster(parallel::detectCores(logical = FALSE),
                       setup_strategy = "sequential")
registerDoParallel(cluster)
Trajectorylist <- foreach(k = 1:nrow(allopt),
                          .errorhandling ="pass",
                          .export=c('init.values.H.1'),
                          .packages = c("data.table","pomp"))%dopar% {                      
                            
    # 1.1. Update parameter list with the alternative parameters ----  
    param.values0[colnames(allopt[,c(5:13)])] <- allopt[k,c(5:13)]
    param.values0$NF <- allopt[k,]$NF0
    param.values0$rhoIHS <- NULL
    param.values0$effectIRS <- NULL
    
    # 2.1.2. Extend the parameter list in age structure format ----
    
    output.stoch<-Getoutput.age(pomp.object= if(allopt[k, model == "Model E0"]) pomp.stoch_E0 else pomp.stoch_E1,
                                param.values=param.values0,
                                initvec = as.numeric(Initcontrol[[1]][k,]),
                                time.window = time.window,
                                mu.H = mu.H,
                                probcat = NH,
                                popsize = as.numeric(allopt[k,]$popsize0),
                                seed = allopt[k,seedi],
                                det.model = FALSE,
                                ageing = TRUE,
                                nsim = 1,
                                combine.erlang = FALSE,
                                rhoIHSa = NULL,
                                rhoIHDa = NULL)
    
     inivector<-as.numeric(output.stoch[time==as.numeric(Timenocases[k,]$Timenocases), statenames.det,with=FALSE])
     outputf=output.stoch[,c("time", "VLinc", "VLtreatinc")]
     
    return(list(inivector,outputf))
  }

stopCluster(cluster)


TPreTNC=do.call(Map, c(f = rbind, Trajectorylist))
save(TPreTNC,file=file.path(output.dir,"TrajectoryPreTimeNoCases.RData"))




